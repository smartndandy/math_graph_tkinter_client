from tkinter import Tk, StringVar, END, Menu, Text, Frame, Label, PanedWindow, LabelFrame, \
                    HORIZONTAL, Button, SEL, LEFT, TOP, RIGHT, BOTTOM, X, Y, BOTH, NONE, PhotoImage
from tkinter import ttk
from communicate import Communicate
from models import ANode, ARelationship

PROGRAM_NAME = "Argute 0.01 Client" 
SELECT_COUNT = 0
ID_COUNTER = 0
ENT1 = None
ENT2 = None
REL_TYPE = None
RELS = []


"""
Helper Functions
"""
####################################################################################################
def search_output(needle, if_ignore_case, content_text_, sel_tag_local, sel_tag_persistent):
    matches_found = 0
    if needle:
        start_pos = '1.0'
        while True:
            start_pos = content_text_.search(needle, start_pos,
                                            nocase=if_ignore_case, stopindex=END)
            if not start_pos:
                break
            end_pos = '{}+{}c'.format(start_pos, len(needle))
            content_text_.tag_add(sel_tag_local, start_pos, end_pos)
            content_text_.tag_add(sel_tag_persistent, start_pos, end_pos)
            matches_found += 1
            start_pos = end_pos
        

def select_all(content_text_, event=None):
    content_text_.tag_add('sel', '1.0', 'end')
    return "break"


#if tag styling is button widget:
"""
button_new = Button(content_text, text=content)
content_text.window_create(first_index, window=button_new)
"""
def select(content_text_, tree_, event=None):
    global SELECT_COUNT
    global ENT1 
    global ENT2

    if SELECT_COUNT == 0:
        content = content_text_.selection_get()
        ENT1 = content

        first_index, second_index = content_text_.tag_ranges(SEL)

        content_text_.tag_add('current_sel1', first_index, second_index)
        content_text_.tag_add("tag"+str(ID_COUNTER)+"_1", first_index, second_index)
        content_text_.tag_config('current_sel1', font="15",  offset="1", borderwidth="1", relief="solid", background="light yellow", foreground="black") 
        search_output(content, True, content_text_, "current_sel1", "tag"+str(ID_COUNTER)+"_1")
    
        print(content)
        SELECT_COUNT += 1

    elif SELECT_COUNT == 1:
        content = content_text_.selection_get()
        ENT2 = content

        first_index, second_index = content_text_.tag_ranges(SEL)

        content_text_.tag_add('current_sel2', first_index, second_index)
        content_text_.tag_add("tag"+str(ID_COUNTER)+"_2", first_index, second_index)
        content_text_.tag_config("current_sel2", font="15",  offset="1", borderwidth="1", relief="solid", background="light green", foreground="black") 
        search_output(content, True, content_text_, "current_sel2", "tag"+str(ID_COUNTER)+"_2")
    
        print(content)

        SELECT_COUNT +=1

    else:
        print("SELECT_COUNT: ", SELECT_COUNT)

    return "break"

def unselect(content_text_, tree_, event=None):
    global SELECT_COUNT
    global ENT1 
    global ENT2

    if SELECT_COUNT == 1:
        content_text_.tag_delete('current_sel1')
        content_text_.tag_delete("tag"+str(ID_COUNTER)+"_1")
        SELECT_COUNT -= 1
        ENT1 = None
    elif SELECT_COUNT == 2:
        content_text_.tag_delete('current_sel2')
        content_text_.tag_delete("tag"+str(ID_COUNTER)+"_2")
        SELECT_COUNT -= 1
        ENT2 = None
    else:
        print("SELECT_COUNT: ", SELECT_COUNT)

    return "break"

#not_of()
def add_descr_rel1(content_text_, tree_, event=None):
    add_relationship("not_of()", content_text_, tree_)
    return "break"

#alias_of
def add_descr_rel2(content_text_, tree_, event=None):
    add_relationship("alias_of()", content_text_, tree_)
    return "break"

#spec_in_con_of
def add_descr_rel3(content_text_, tree_, event=None):
    add_relationship("spec_in_con_of()", content_text_, tree_)
    return "break"

#eq_in_con
def add_descr_rel4(content_text_, tree_, event=None):
    add_relationship("eq_in_con()", content_text_, tree_)
    return "break"

#def_in_con
def add_descr_rel5(content_text_, tree_, event=None):
    add_relationship("def_in_con()", content_text_, tree_)
    return "break"


def add_relationship(relationship_type, content_text_, tree_):
    global SELECT_COUNT
    global ID_COUNTER
    global ENT1
    global ENT2
    

    tree_.insert("","end", "id" + str(ID_COUNTER), text="id" + str(ID_COUNTER), values=(ENT1, relationship_type, ENT2))
    
    """
    ent1_1, ent1_2 = content_text_.tag_ranges("current_sel1")
    ent2_1, ent2_2 = content_text_.tag_ranges("current_sel2")
    """
    a_node1 = ANode(ENT1, "id" + str(ID_COUNTER))
    a_node2 = ANode(ENT2, "id" + str(ID_COUNTER))
    a_relationship = ARelationship(relationship_type, "id" + str(ID_COUNTER), a_node1, a_node2)
    RELS.append(a_relationship)

    content_text_.tag_config("tag"+str(ID_COUNTER)+"_1", font="15",  offset="1", underline=1, background="light grey") 
    content_text_.tag_config("tag"+str(ID_COUNTER)+"_2", font="15",  offset="1", underline=1, background="light grey") 
    ID_COUNTER += 1

    content_text_.tag_delete('current_sel1')
    content_text_.tag_delete('current_sel2')
    SELECT_COUNT = 0
    ENT1=None
    ENT2=None


def show_popup_menu(event, popup_menu_):
    popup_menu_.tk_popup(event.x_root, event.y_root)

def push_rels_to_db(tree_):

    nodes = []
    for rel in RELS:
        nodes.append(rel.get_node1())
        nodes.append(rel.get_node2())

    comm = Communicate()
    comm.post_data(RELS, nodes)


####################################################################################################
"""
End of helper functions
"""
"""
    for i in ('select','unselect', "add_rel1", "add_rel2", "add_rel3"):
        cmd = eval(i)
        helper = lambda : cmd(content_text, tree)
        popup_menu.add_command(label=i, compound='left', command=helper)
"""

def configure_left_pane(left_pane_):
    
    ###pane left content
    content_text = Text(left_pane_, wrap="word", undo=1, font="14")
    label1 = Label(left_pane_, text="Enter text:").grid(row=0, column=1, sticky="ws",pady="10")
    frame_left = Frame(left_pane_,width="30")
    frame_left.grid(row=0, column=0, rowspan=10, sticky="ewns")

    
    content_text.grid(row=1, column=1, columnspan=4, rowspan=9, sticky="es") 

    return content_text

def configure_right_pane(right_pane_):
    
     ### pane right content
    label2 = Label(right_pane_, text="These relationships are added:").grid(row=0, column=0, sticky="ws",pady="10")
    tree = ttk.Treeview(right_pane_, columns=("entity1", "relationship type", "entity2"))
    tree.column("entity1", width=140, anchor="center")
    tree.heading("entity1", text="entity1")
    tree.column("relationship type", width=160, anchor="center")
    tree.heading("relationship type", text="relationship type")
    tree.column("entity2", width=140, anchor="center")
    tree.heading("entity2", text="entity2")
    tree.column("#0", width=50)
    tree.heading("#0", text="id:")
    tree.grid(row=1, column=0)

    return tree


def build_gui(parent):

    #Menu Bar
    menu_bar = Menu(parent) 
    about_menu= Menu(menu_bar, tearoff=0)
    about_menu.add_command(label='About')
    about_menu.add_command(label='Help')
    menu_bar.add_cascade(label="About", menu=about_menu)
    parent.config(menu=menu_bar)



    """
    The following buttons/functionalities are needed:
    - sync (text in db? entities in db? -> shown in bottom bar)
    - save to db 
    - save to file
    - open file
    - show all rels and entities (text) (rels and entities are deleted through tree)
    """
    #client\new_file.gif

    new_file_icon = PhotoImage(file="client/icons/save.gif")
    """
    open_file_icon = PhotoImage(file='icons/open_file.gif')
    save_file_icon = PhotoImage(file='icons/save.gif')
    cut_icon = PhotoImage(file='icons/cut.gif')
    copy_icon = PhotoImage(file='icons/copy.gif')
    paste_icon = PhotoImage(file='icons/paste.gif')
    undo_icon = PhotoImage(file='icons/undo.gif')
    redo_icon = PhotoImage(file='icons/redo.gif')
    """

    #Main left frame
    frame_left = Frame(parent,background='gray80', width=50)
    #Buttons
    #button1 = Button(frame_left, command=lambda: push_rels_to_db(tree), text="Push")
    #button1.grid(row=1,column=9, sticky="we")
    #button1.pack(side=TOP)
    frame_left.pack(expand="no", fill="y", side="left")
    button1= Button(frame_left,image=new_file_icon,command=lambda: push_rels_to_db(tree),width=40,height=40)
    button1.image = new_file_icon
    button1.pack(side="top")

    """
    file_menu.add_command(label='New', accelerator='Ctrl+N', compound='left',
                      image=new_file_icon, underline=0, command=new_file)

    frame_button1 = Frame(frame_left,width=50,height=50, background="light green")
    frame_button1.grid_propagate(False)
    frame_button1.columnconfigure(0, weight=1)
    frame_button1.rowconfigure(0, weight=0)
    frame_button1.grid(row=0, column=0)

    button1 = Button(frame_button1, command=lambda: push_rels_to_db(tree))
    button1.grid()
    """

    #Paned Window
    paned_window = PanedWindow(parent, orient=HORIZONTAL, width=1000, height=700)
    left_pane = LabelFrame(paned_window, text="Pane Left") #width="400", height="800"
    right_pane = LabelFrame(paned_window, text="Pane Right") #width="500", height="800"
    content_text = configure_left_pane(left_pane)
    tree = configure_right_pane(right_pane)
    paned_window.add(left_pane)
    paned_window.add(right_pane)
    #paned_window.grid(row=0, column=0, columnspan=10, sticky="wsne")
    paned_window.pack(side=TOP, fill=X, expand=1)


    
    #Popup Menu
    popup_menu = Menu(content_text, cursor="top_left_arrow", tearoff=0)
    descr_rels_menu = Menu(popup_menu, tearoff=0)
    comp_rels_menu = Menu(popup_menu, tearoff=0)
    cat_rels_menu = Menu(popup_menu, tearoff=0)
    func_rels_menu = Menu(popup_menu, tearoff=0)
    popup_menu.add_cascade(label="Descriptive Relationships", menu=descr_rels_menu)
    popup_menu.add_cascade(label="Compositional Relationships", menu=comp_rels_menu)
    popup_menu.add_cascade(label="Categorical Relationships", menu=cat_rels_menu)
    popup_menu.add_cascade(label="Functional Relationships", menu=func_rels_menu)

    descr_rels_menu.add_cascade(label="not_of()", command = lambda : add_descr_rel1(content_text, tree))
    descr_rels_menu.add_cascade(label="alias_of()", command = lambda : add_descr_rel2(content_text, tree))
    descr_rels_menu.add_cascade(label="spec_in_con_of()", command = lambda : add_descr_rel3(content_text, tree))
    descr_rels_menu.add_cascade(label="eq_in_con()", command = lambda : add_descr_rel4(content_text, tree))
    descr_rels_menu.add_cascade(label="def_in_con()", command = lambda : add_descr_rel5(content_text, tree))

    popup_menu.add_separator()
    popup_menu.add_command(label="select", command = lambda : select(content_text, tree))
    popup_menu.add_command(label="unselect", command = lambda : unselect(content_text, tree))
    popup_menu.add_command(label="Select All", underline=7, command=lambda event: select_all(content_text, tree, event))

    event_adder = lambda event : show_popup_menu(event, popup_menu)
    content_text.bind('<Button-3>', event_adder) 

    

def execute_app():
    
    root = Tk()
    root.minsize(width=1200, height=550) 
    root.title(PROGRAM_NAME)
    build_gui(root)
    root.mainloop()

if __name__ == "__main__":
    
    execute_app()



