PROGRAM_NAME = "Argute 0.01 Client" 

RELATIONSHIP_CATEGORIES = [ "Descriptive Relationships", \
                            "Compositional Relationships", \
                            "Categorical Relationships", \
                            "Functional Relationships" ]

DESCR_RELS_REGULAR = [("has_notation", "notation_of"),\
                      ("has_alias", "alias_of"), \
                      ("has_spec_in_con", "spec_in_con"), \
                      ("eq_in_con", "eq_in_con"),  \
                      ("has_def_in_con", "def_in_con") ] 

COMP_RELS_REGULAR = [("has_atom", "atom_of"), \
                     ("has_unc_ins", "unc_ins_of"), \
                     ("has_cer_ins", "cer_ins_of"), \
                     ("has_quantif", "quantif_of"), \
                     ("has_subset", "subset_of")]

CAT_RELS_REGULAR = [("has_subcat","subcat_of")]

FUNC_RELS_REGULAR = [("has_atr", "atr_of"), \
                     ("has_prop", "prop_of"), \
                     ("has_operator", "operator_of"), \
                     ("sem_equiv", "sem_equiv")]



CONN_STATUS = ["active", "not active"]