from tkinter import Tk, StringVar, END, Menu, Text, Frame, Label, PanedWindow, LabelFrame, Entry, \
                    HORIZONTAL, VERTICAL, Button, SEL, LEFT, TOP, RIGHT, BOTTOM, X, Y, BOTH, NONE, PhotoImage, Scrollbar, Toplevel, filedialog
from tkinter import ttk, messagebox, OptionMenu
from functools import partial

from arg_constants import *
import models
import controller
import os
import pickle 
import threading
import time
import json


class View:

    class RelTree:

        def __init__(self, parent, outer_class):
            self.tree = None
            self.configure_tree(parent)
            self.outer_class = outer_class

        def configure_tree(self, parent):

            ################################################################################
            # Tkinter TreeView tagconfig fix
            ################################################################################
            style = ttk.Style()
            style.map('Treeview', foreground=self.fixed_map('foreground'),
            background=self.fixed_map('background'))
            ################################################################################
            
            self.tree = ttk.Treeview(parent, columns=("id", "entity1", "relationship type", "entity2", "constraints"))
            self.tree.column("id", width=40, anchor="center")
            self.tree.heading("id", text="id")
            self.tree.column("entity1", width=110, anchor="center")
            self.tree.heading("entity1", text="entity1")
            self.tree.column("relationship type", width=130, anchor="center")
            self.tree.heading("relationship type", text="relationship type")
            self.tree.column("entity2", width=110, anchor="center")
            self.tree.heading("entity2", text="entity2")
            self.tree.column("constraints", width=100, anchor="center")

            
            
            self.tree.heading("constraints", text="constraints")
            self.tree.column("#0", width="0")
            self.tree.heading("#0", text="")
            self.tree.pack(side="top", fill="both", expand=1)

        def add_relationship(self, id_counter, relationship_type, ent1, ent2, constraint=""):
            self.tree.insert("","end", values=("id"+str(id_counter), ent1, relationship_type, ent2, constraint))
            #self.tree.tag_bind('bg', '<1>', self.itemClicked) 


        def itemClicked(self, event=None):
            focused = self.tree.focus()
            item_index = int(str(focused)[1:])
            temp_list = ["" for x in range(5)]
            count = 0
            for elem in self.tree.item(focused)["values"]:
                temp_list[count] = elem
                count += 1

            constraint_value = self.outer_class.exception_var.get()
            self.tree.insert("", int(str(focused)[1:]), values=(temp_list[0], temp_list[1], temp_list[2], temp_list[3], temp_list[4]+constraint_value))
            self.outer_class.controller.add_constraint_to_rel_with_id(temp_list[0], temp_list[4]+constraint_value)
            self.tree.delete(focused)
            """
            print(event, self.tree.selection(), self.tree.focus())
            self.tree.tag_configure('bg', background='green')
            #self.tree.tag_configure('fg', foreground="red")
            """

        def add_relationships_from_gdoc(self, sem_rels):  
            for rel in sem_rels:
                id_counter = rel.get_rel_id()
                rel_type = rel.get_a_type()
                ent1 = rel.get_node1().get_term_type()
                ent2 = rel.get_node2().get_term_type()
                constraint = rel.get_constraints()
                self.add_relationship(id_counter, rel_type, ent1, ent2, constraint)

        def tag_configure(self, tag, b_colour):
            self.tree.tag_configure(tag, background=b_colour)
            
        def fixed_map(self, option):
            # Fix for setting text colour for Tkinter 8.6.9
            # From: https://core.tcl.tk/tk/info/509cafafae
            #
            # Returns the style map for 'option' with any styles starting with
            # ('!disabled', '!selected', ...) filtered out.

            # style.map() returns an empty list for missing options, so this
            # should be future-safe.
            style = ttk.Style()
            return [elm for elm in style.map('Treeview', query_opt=option) if
                elm[:2] != ('!disabled', '!selected')]


    class RelTypes:

        def __init__(self):
            
            self.default_rel_types_path = "client/configs/default_sem_types.json"
            self.rel_types_path = "client/configs/sem_types.json"

            self.direction = "child to parent"
            self.option_list = ["parent to child", "child to parent"]

            # dictionaries - like json
            self.default_rel_types = None 
            self.rel_types = None 

            self.current_rel_types_list = []
            self.current_rel_type_categories = [] #list of category name strings
            self.current_rel_types = []           #list of dictionaries with pairs of rel types

        def set_direction(self, direction):
            self.direction = direction

        def is_direction_p_to_ch(self):
            if self.direction == self.option_list[0]:
                return True 
            elif self.direction == self.option_list[1]:
                return False

        def load_rel_types_from_file(self):
            
            with open("client/configs/default_sem_types.json", "r") as fp:
                self.default_rel_types = json.load(fp)
            fp.close()
        
            try:
                with open("client/configs/sem_types.json", "r") as fp:
                    self.rel_types = json.load(fp) 
                fp.close()
            except:
                fp.close()
                self.rel_types = {}

            if len(self.rel_types) == 0:
                self.extract_from_file_dict(self.default_rel_types)
            else:
                print("RelTypes --- Doing something else!")
                self.extract_from_file_dict(self.rel_types)


        def extract_from_file_dict(self, source_dict):

            for key,value in source_dict.items():
                self.current_rel_types.append(value)

            for elem in self.current_rel_types:
                for key,value in elem.items():
                    self.current_rel_type_categories.append(key)

            temp = {}
            for elem in self.current_rel_types:
                for key,value in elem.items():
                    temp[key] = value 
            self.current_rel_types = temp



        def set_category_types(self, category_names):
            
            self.current_rel_type_categories = list(category_names) 


        def set_current_rel_types_for_category(self, rel_types_list, category):
            temp = {}
            temp[category] = rel_types_list
            self.current_rel_types_list.append(temp)

        def compose_current_rel_types(self):
            self.rel_types = {} 
            count = 0
            for elem in self.current_rel_types_list:
                self.rel_types[str(count)] = elem 
                count += 1 

            self.current_rel_types = {}
            for elem in self.current_rel_types_list:
                for el in elem:
                    self.current_rel_types[el] = elem[el]
            self.current_rel_types_list = []

            



        def get_current_rel_type_categories(self):
            return self.current_rel_type_categories


        def get_current_rel_types_for_category(self, category, direction):
            
            temp_list = self.current_rel_types[category]
            temp_str = ""
            #direction_list = temp_list.values()

            for elem in temp_list:
                for el in elem:
                    if el == direction:
                        temp_str += (elem[el] + ";")
           
            return temp_str
    
        def get_current_rel_types_list_for_category(self, category, direction):
            
            temp_list = self.current_rel_types[category]
            return_list = []
            #direction_list = temp_list.values()

            for elem in temp_list:
                for el in elem:
                    if el == direction:
                        return_list.append(elem[el])

            return return_list

        def save_current_rel_types_to_file(self):

            self.compose_current_rel_types()

            try:
                with open(self.rel_types_path, "w") as fp:
                    fp.seek(0)
                    json.dump(self.rel_types, fp, indent=4)
                fp.close()
            except:
                fp.close()

        
    
    def __init__(self, parent, controller):
        self.SELECT_COUNT = 0 
        self.ID_COUNTER = 0
        self.ENT1 = None
        self.ENT2 = None 
        self.REL_TYPE = None 

        self.controller = controller
        self.selections = []
        self.rel_tree = None

        self.frame_left = None
        self.frame_right = None

        self.content_text = None
        self.answer_text = None
        self.left_pane = None 
        self.right_pane = None
        self.parent = parent

        self.ip = None 
        self.port = None 
        self.url_suffix = None

        self.occurences_manipulatable = False

        self.connection_status_label = None 
        self.rels_count_label = None 
        self.nodes_count_label = None 
        self.last_action_label = None

        # RelTypes object
        self.rel_type_direction = None
        self.rel_types = View.RelTypes()
        self.rel_types.load_rel_types_from_file()
    
        self.configure_parent()
        self.create_app_base()


    def configure_parent(self):
        self.parent.title(PROGRAM_NAME)
        self.parent.minsize(width=1400, height=600)

    def create_app_base(self):
        self.create_menu_bar()
        self.create_bottom_bar()
        self.create_side_bar() 
        self.create_paned_window()
    
    def create_menu_bar(self): 
        #Menu Bar
        menu_bar = Menu(self.parent) 
        about_menu= Menu(menu_bar, tearoff=0)
        edit_menu = Menu(menu_bar, tearoff=0)
        config_menu = Menu(menu_bar, tearoff=0)

        menu_bar.add_cascade(label="Edit", menu=edit_menu)
        edit_menu.add_command(label="Save (file)", underline=0, command = self.save_to_file)
        edit_menu.add_command(label="Open (file)", underline=0, command = self.load_from_file)

        config_menu.add_command(label="Database connection", underline=0, accelerator="Ctrl+D",command=self.setup_connection)
        config_menu.add_command(label="Relationship types", underline=0, accelerator="Ctrl+T", command=self.set_sem_types)
        menu_bar.add_cascade(label="Configuration", menu=config_menu)

        about_menu.add_command(label='About')
        about_menu.add_command(label='Help')
        menu_bar.add_cascade(label="About", menu=about_menu)

        self.parent.bind('<Control-d>', self.setup_connection)
        self.parent.bind('<Control-D>', self.setup_connection)
        self.parent.bind('<Control-t>', self.set_sem_types)
        self.parent.bind('<Control-T>', self.set_sem_types)

        self.parent.config(menu=menu_bar)

    def create_side_bar(self):
        save_file_icon = PhotoImage(file="client/icons/file_save35.png")
        save_db_icon = PhotoImage(file="client/icons/db_upload_35.png")
        analyze_db_icon = PhotoImage(file="client/icons/db_analyze35.png")
        load_from_db_icon = PhotoImage(file="client/icons/db_download_35.png")
        sync_meta_icon = PhotoImage(file="client/icons/sync35.png")

        self.frame_left = Frame(self.parent,background='gray80', width=50)
        self.frame_right = Frame(self.parent,background="gray60")
        self.frame_left.pack(expand="no", fill="y", side="left")
        self.frame_right.pack(expand="yes",fill="both", side="left")

        # Save to file 
        button1= Button(self.frame_left,image=save_file_icon,command=lambda: self.save_to_file(),width=40,height=40)
        button1.image = save_file_icon
        button1.pack(side="top")
        separator = ttk.Separator(self.frame_left, orient="horizontal")
        separator.pack(side="top", pady="4", fill="x")

        # Save to database 
        button2= Button(self.frame_left,image=save_db_icon,command=lambda: self.push_data(),width=40,height=40)
        button2.image = save_db_icon
        button2.pack(side="top")

        # Analyze/ Sync with database
        button3= Button(self.frame_left,image=analyze_db_icon,command=lambda: self.sync_data(),width=40,height=40)
        button3.image = analyze_db_icon
        button3.pack(side="top")
        separator = ttk.Separator(self.frame_left, orient="horizontal")
        separator.pack(side="top", pady="4", fill="x")

        # Load data - execute query
        button4= Button(self.frame_left,image=load_from_db_icon,command=lambda: self.execute_query(),width=40,height=40)
        button4.image = load_from_db_icon
        button4.pack(side="top")

        # sync graph meta
        separator1 = ttk.Separator(self.frame_left, orient="horizontal")
        separator1.pack(side="bottom", pady="2", fill="x")
        button5 = Button(self.frame_left,image=sync_meta_icon,command=lambda: self.sync_graph_meta(),width=40,height=40)
        button5.image = sync_meta_icon
        button5.pack(side="bottom")

    def create_paned_window(self):
        self.left_pane = LabelFrame(self.frame_right, text="Editor") #width="400", height="800"
        self.right_pane = LabelFrame(self.frame_right, text="Detail View", width="490") #width="500", height="800"
        self.create_left_pane()
        self.create_right_pane()
        
        self.left_pane.pack(expand="yes", fill="both", side="left")
        self.right_pane.pack(expand="no", fill="y", side="left")

    def create_left_pane(self):
        
        p = ttk.Panedwindow(self.left_pane, orient=VERTICAL)
        # two panes, each of which would get widgets gridded into it:
        f1 = ttk.Labelframe(self.left_pane, text='Input')
        f2 = ttk.Labelframe(self.left_pane, text='Output')   

        self.content_text = Text(f1, wrap="word", undo=1, font=13, height=30)  
        self.content_text.pack(expand=1, fill="both", side="top")
        self.answer_text = Text(f2, wrap="word", undo=1, font=13, height=24)
        self.answer_text.pack(expand=1, fill="both", side="top")

        p.add(f1)
        p.add(f2)
        p.pack(fill="both", side="top")
        self.content_text.configure(font=("Arial",12))
        self.answer_text.configure(font=("Arial",12))
        self.create_popup_menu()

    def create_right_pane(self):

        p_right = ttk.PanedWindow(self.right_pane, orient=VERTICAL)

        f1 = ttk.LabelFrame(self.right_pane) 
        f2 = ttk.LabelFrame(self.right_pane, text="Semantic Relations")

        details_frame = Frame(f1 ,background='grey93', height="800")

        self.document_name_v = StringVar()
        document_name_label = Label(details_frame, text="document name:")
        self.document_name_entry = Entry(details_frame, width=61, textvariable=self.document_name_v)
        self.chapter_name_v = StringVar()
        chapter_name_label = Label(details_frame, text="chapter name:")
        self.chapter_name_entry = Entry(details_frame, width=61, textvariable=self.chapter_name_v)
        self.sections_number_v= StringVar()
        sections_number_label = Label(details_frame, text="number of sections:")
        self.sections_number_entry = Entry(details_frame, width=61, textvariable=self.sections_number_v)
        self.sentences_number_v = StringVar()
        sentences_number_label = Label(details_frame, text="number of sentences:")
        self.sentences_number_entry = Entry(details_frame, width=61, textvariable=self.sentences_number_v)

        sep1 = ttk.Separator(details_frame, orient=HORIZONTAL)
        sep0 = ttk.Separator(details_frame, orient=HORIZONTAL)
        section_intentions_label = Label(details_frame, text="section intentions:")
        self.section_intentions_entry = Text(details_frame, height=6, width=41, font=10)

        sep2 = ttk.Separator(details_frame, orient=HORIZONTAL)
        sentences_intentions_label = Label(details_frame, text="sentences intentions:")
        self.sentences_intentions_entry = Text(details_frame, height=6, width=41, font=10)


        sep0.grid(row=0, column=0, columnspan=2, sticky="ew")
        document_name_label.grid(row=1, column=0, pady=2, sticky="w")
        self.document_name_entry.grid(row=1, column=1, pady=2, sticky="ew")
    
        chapter_name_label.grid(row=2, column=0, pady=2, sticky="w")
        self.chapter_name_entry.grid(row=2, column=1, pady=2, sticky="ew")
        sections_number_label.grid(row=3, column=0, pady=2, sticky="w")
        self.sections_number_entry.grid(row=3, column=1, pady=2, sticky="ew")
        sentences_number_label.grid(row=4, column=0, pady=2, sticky="w")
        self.sentences_number_entry.grid(row=4, column=1, pady=2, sticky="ew")
        
        sep1.grid(row=7, column=0, columnspan=2, sticky="ew")
        
        section_intentions_label.grid(row=8, column=0, pady=2, sticky="wn")
        self.section_intentions_entry.grid(row=8, column=1, pady=2, sticky="e")
        sep2.grid(row=9, column=0, columnspan=2, sticky="ew")
        sentences_intentions_label.grid(row=10, column=0, pady=2, sticky="wn")
        self.sentences_intentions_entry.grid(row=10, column=1, pady=2, sticky="e")
        

        details_frame.grid_columnconfigure(0, weight=1)
        details_frame.grid_columnconfigure(1, weight=2)


        details_frame.pack(fill="both", side="top")
        
    
        self.rel_exception_label = Label(f2, text="Enter a constraint or constraint exception: ") 
        self.rel_frame = Frame(f2, height="5")
        self.exception_var = StringVar()
        self.rel_exception_editor = Entry(self.rel_frame, width=70, textvariable=self.exception_var)
        self.rel_exception_button = Button(self.rel_frame, width=10, height=1, text="Add")

        self.rel_exception_label.pack(side="top", fill="x")
        self.rel_frame.pack(side="top", fill="x")
        
        self.rel_exception_button.pack(side="right", anchor="e")
        self.rel_exception_editor.pack(side="left", anchor="w", fill="x")
        #self.rel_exception
        self.rel_tree = self.RelTree(f2, self)
        self.rel_exception_button.configure(command=self.rel_tree.itemClicked)

        p_right.add(f1)
        p_right.add(f2)
        p_right.pack(fill="both", side="top", expand=1)
        

    def create_bottom_bar(self):
        bottom_bar = Frame(self.parent,  height=25, background='SteelBlue1')
        bottom_bar.pack(expand='no', fill='x', side="bottom")

        #connection status
        self.connection_status_label = Label(bottom_bar, text="not active", background='SteelBlue1', foreground="red", font="Helvetica 11 bold" )
        self.connection_status_label.pack(expand="no", side="right")
        connection_status_label_label = Label(bottom_bar, text="Connection:", background='SteelBlue1', foreground="black", font="Helvetica 11 bold" )
        #connection_label.configure(text = "No connection" )
        connection_status_label_label.pack(expand="no", side="right")
        separator_style = ttk.Style()
        separator_style.configure("Line.TSeparator", background="yellow")
        sep = ttk.Separator(bottom_bar, orient=VERTICAL, style="Line.TSeparator")
        sep.pack(expand="no", side="right", fill="y", padx="4")

        #db count status
        self.nodes_count_label = Label(bottom_bar, text=" <nodes: 0>", background='SteelBlue1', foreground="black", font="Helvetica 11 bold")
        self.rels_count_label = Label(bottom_bar, text=" <rels: 0>", background="SteelBlue1", foreground="black", font="Helvetica 11 bold")
        nodes_rels_count_label = Label(bottom_bar, text="DB:", background='SteelBlue1', foreground="black", font="Helvetica 11 bold" )
        self.nodes_count_label.pack(expand="no", side="right")
        self.rels_count_label.pack(expand="no", side="right")
        nodes_rels_count_label.pack(expand="no", side="right")
        sep1 = ttk.Separator(bottom_bar, orient=VERTICAL, style="Line.TSeparator")
        sep1.pack(expand="no", side="right", fill="y", padx="4")

        #last action status
        self.last_action_label = Label(bottom_bar, text="Last action: client started", background='SteelBlue1', foreground="black", font="Helvetica 11 bold" )
        self.last_action_label.pack(expand="no", side="left")
        sep2 = ttk.Separator(bottom_bar, orient=VERTICAL, style="Line.TSeparator")
        sep2.pack(expand="no", side="left", fill="y", padx="4")

    def popup_menu_update(self):

        self.popup_menu.delete(0,"end")
        self.popup_menu_add_commands()

    def popup_menu_add_commands(self):

        self.popup_menu.add_command(label="select", command = lambda : self.select())
        self.popup_menu.add_command(label="unselect", command = lambda : self.unselect())
        self.popup_menu.add_separator()
        category_name = self.rel_types.get_current_rel_type_categories()
        menus = []
        count = 0

        # self.rel_types.get_current_rel_types_for_category(str(category_name_str[0]), "p_to_ch")

        for elem in category_name:
            """
            rels1_str_l = self.rels1_entry_widget.get().split(";")
            rels1_str_inverted_l = self.rels1_entry_widget_inverted.get().split(";") 
            #rels1_str_l = rels1_str_l[::-1]
            #rels1_str_inverted_l = rels1_str_inverted_l[::-1]
            rels1_str_l.pop() 
            rels1_str_inverted_l.pop()
            """

            p_to_ch_list = self.rel_types.get_current_rel_types_for_category(elem, "p_to_ch").split(";")
            ch_to_p_list = self.rel_types.get_current_rel_types_for_category(elem, "ch_to_p").split(";")
            p_to_ch_list.pop() 
            ch_to_p_list.pop()

            rel_list = []
            if self.rel_types.is_direction_p_to_ch():
                rel_list = p_to_ch_list
            else:
                rel_list = ch_to_p_list

            new_menu = Menu(self.popup_menu, tearoff=0)
            func_dict = {f"func{rel}": partial(self.add_relationship, relationship_type=rel) for rel in rel_list}
            for rel in rel_list:
                this_func = func_dict["func"+rel]
                new_menu.add_cascade(label=rel, command = this_func)

            """
            def add_descr_rel3(self, event=None):
                self.add_relationship(DESCR_RELS[2])
                return "break"
            """

            menus.append(new_menu)

            self.popup_menu.add_cascade(label=category_name[count], menu=new_menu)

            #self.rel_types.get_current_rel_types_for_category(str(category_name_str[0]), "p_to_ch")

            count += 1

        
        """
        self.popup_menu.add_command(label="show occurences", command = lambda : self.show_occurences())
        self.popup_menu.add_command(label="stop showing occurences", command = lambda : self.stop_showing_occurences())
        self.popup_menu.add_separator()
        """

    def some_val(self, event):

        print(str(event))

    def create_popup_menu(self):
        #Popup Menu
        self.popup_menu = Menu(self.content_text, cursor="top_left_arrow", tearoff=0)
        self.popup_menu_add_commands()
        
        event_adder = lambda event : self.show_popup_menu(event, self.popup_menu)
        self.content_text.bind('<Button-3>', event_adder) 

    

    def set_rel_type_values(self):
        
        print("Setting values !!!") 

        category_names = ["", "", "", ""]
        ignore_list = ["", " ", "  ", "   ", "\n"]
        # category names ------------------
        type1_val = self.type1_entry_widget.get() 
        if type1_val not in ignore_list:
            category_names[0] = type1_val

            rels1_str_l = self.rels1_entry_widget.get().split(";")
            rels1_str_inverted_l = self.rels1_entry_widget_inverted.get().split(";") 
            #rels1_str_l = rels1_str_l[::-1]
            #rels1_str_inverted_l = rels1_str_inverted_l[::-1]
            rels1_str_l.pop() 
            rels1_str_inverted_l.pop()
            rels1_types_comp_l = []
            for i in range(len(rels1_str_l)):
                temp_dict = {} 
                temp_dict["p_to_ch"] = rels1_str_l[i]
                temp_dict["ch_to_p"] = rels1_str_inverted_l[i] 
                rels1_types_comp_l.append(temp_dict)
            self.rel_types.set_current_rel_types_for_category(rels1_types_comp_l, category_names[0])
        else:
            category_names.pop()

        type2_val = self.type2_entry_widget.get() 
        if type2_val not in ignore_list:
            category_names[1] = type2_val

            rels2_str_l = self.rels2_entry_widget.get().split(";")
            rels2_str_inverted_l = self.rels2_entry_widget_inverted.get().split(";") 
            #rels2_str_l = rels2_str_l[::-1] 
            #rels2_str_inverted_l = rels2_str_inverted_l[::-1]
            rels2_str_l.pop()
            rels2_str_inverted_l.pop()

            rels2_types_comp_l = [] 
            for i in range(len(rels2_str_l)):
                temp_dict = {}
                temp_dict["p_to_ch"] = rels2_str_l[i] 
                temp_dict["ch_to_p"] = rels2_str_inverted_l[i] 
                rels2_types_comp_l.append(temp_dict) 
            self.rel_types.set_current_rel_types_for_category(rels2_types_comp_l, category_names[1])
        else:
            category_names.pop()

        type3_val = self.type3_entry_widget.get() 
        if type3_val not in ignore_list:
            category_names[2] = type3_val

            rels3_str_l = self.rels3_entry_widget.get().split(";")
            rels3_str_inverted_l = self.rels3_entry_widget_inverted.get().split(";")
            #rels3_str_l = rels3_str_l[::-1] 
            #rels3_str_inverted_l = rels3_str_inverted_l[::-1]
            rels3_str_l.pop() 
            rels3_str_inverted_l.pop()

            rels3_types_comp_l = [] 
            for i in range(len(rels3_str_l)):
                temp_dict = {}
                temp_dict["p_to_ch"] = rels3_str_l[i] 
                temp_dict["ch_to_p"] = rels3_str_inverted_l[i] 
                rels3_types_comp_l.append(temp_dict) 
            self.rel_types.set_current_rel_types_for_category(rels3_types_comp_l, category_names[2])
        else:
            category_names.pop()

        type4_val = self.type4_entry_widget.get() 
        if type4_val not in ignore_list:
            category_names[3] = type4_val

            rels4_str_l = self.rels4_entry_widget.get().split(";")
            rels4_str_inverted_l = self.rels4_entry_widget_inverted.get().split(";")
            #rels4_str_l = rels4_str_l[::-1] 
            #rels4_str_inverted_l = rels4_str_inverted_l[::-1]
            rels4_str_l.pop() 
            rels4_str_inverted_l.pop()

            rels4_types_comp_l = [] 
            for i in range(len(rels4_str_l)):
                temp_dict = {}
                temp_dict["p_to_ch"] = rels4_str_l[i] 
                temp_dict["ch_to_p"] = rels4_str_inverted_l[i] 
                rels4_types_comp_l.append(temp_dict) 
            self.rel_types.set_current_rel_types_for_category(rels4_types_comp_l, category_names[3])
        else:
            category_names.pop()

        self.rel_types.set_direction(self.variable.get()) #get direction value from option menu
        self.rel_types.set_category_types(category_names)
        # /// category names ------------------

        self.rel_types.save_current_rel_types_to_file()
        self.popup_menu_update()
        self.sem_types_toplevel.destroy()


    def set_sem_types(self, event=None):
        self.sem_types_toplevel = Toplevel(self.parent)
        self.sem_types_toplevel.title("Relationship types settings")
        self.sem_types_toplevel.minsize(width=820, height=240)
        self.sem_types_toplevel.maxsize(width=820, height=240)
        self.sem_types_toplevel.transient(self.parent)

        category_name_str = self.rel_types.get_current_rel_type_categories()
        category_name = [StringVar(self.parent, value="") for x in range(len(category_name_str))]
        count = 0
        for elem in category_name_str:
            category_name[count].set(elem) 
            count += 1 

        ### Rel type category 1
        Label(self.sem_types_toplevel, text="Type1: ").grid(row=0,column=0,sticky='e')
        self.type1_entry_widget = Entry(self.sem_types_toplevel, width=25, textvariable=category_name[0])
        self.type1_entry_widget.grid(row=0, column=1, padx=2, pady=2, sticky='we')
        # parent -> child
        rel_type1_p_to_ch = StringVar(self.parent)
        rel_type1_p_to_ch.set(self.rel_types.get_current_rel_types_for_category(str(category_name_str[0]), "p_to_ch"))
        self.rels1_entry_widget = Entry(self.sem_types_toplevel, width=100, textvariable=rel_type1_p_to_ch)
        #self.rels1_entry_widget.configure({"background": "yellow"})
        self.rels1_entry_widget.grid(row=0, column=2, padx=2, pady=2, sticky="we")
        # child -> parent
        rel_type1_ch_to_p = StringVar(self.parent) 
        rel_type1_ch_to_p.set(self.rel_types.get_current_rel_types_for_category(str(category_name_str[0]), "ch_to_p"))
        self.rels1_entry_widget_inverted = Entry(self.sem_types_toplevel, width=100, textvariable=rel_type1_ch_to_p)
        self.rels1_entry_widget_inverted.grid(row=1, column=2, padx=2, pady=2, sticky="we")
        self.type1_entry_widget.focus_set()

        ### Rel type category 2
        Label(self.sem_types_toplevel, text="Type2: ").grid(row=2,column=0,sticky='e')
        self.type2_entry_widget = Entry(self.sem_types_toplevel, width=25)
        self.rels2_entry_widget = Entry(self.sem_types_toplevel, width=100)
        self.rels2_entry_widget_inverted = Entry(self.sem_types_toplevel, width=100)

        if len(category_name) > 1:
            self.type2_entry_widget.config(textvariable=category_name[1])
            self.type2_entry_widget.grid(row=2, column=1, padx=2, pady=2, sticky='we')
            # parent -> child
            rel_type2_p_to_ch = StringVar(self.parent)
            rel_type2_p_to_ch.set(self.rel_types.get_current_rel_types_for_category(str(category_name_str[1]), "p_to_ch"))
            #self.rels2_entry_widget = Entry(self.sem_types_toplevel, width=100, textvariable=rel_type2_p_to_ch)
            self.rels2_entry_widget.config(textvariable=rel_type2_p_to_ch)
            self.rels2_entry_widget.grid(row=2, column=2, padx=2, pady=2, sticky="we")
            # child -> parent
            rel_type2_ch_to_p = StringVar(self.parent) 
            rel_type2_ch_to_p.set(self.rel_types.get_current_rel_types_for_category(str(category_name_str[1]), "ch_to_p"))
            self.rels2_entry_widget_inverted.config(textvariable=rel_type2_ch_to_p)
            #self.rels2_entry_widget_inverted = Entry(self.sem_types_toplevel, width=100, textvariable=rel_type2_ch_to_p)
            self.rels2_entry_widget_inverted.grid(row=3, column=2, padx=2, pady=2, sticky="we")

        ### Rel type category 3
        Label(self.sem_types_toplevel, text="Type3: ").grid(row=4,column=0,sticky='e')
        self.type3_entry_widget = Entry(self.sem_types_toplevel, width=25)
        self.rels3_entry_widget = Entry(self.sem_types_toplevel, width=100)
        self.rels3_entry_widget_inverted = Entry(self.sem_types_toplevel, width=100)

        if len(category_name) > 2:
            self.type3_entry_widget.config(textvariable=category_name[2])
            self.type3_entry_widget.grid(row=4, column=1, padx=2, pady=2, sticky='we')
            # parent -> child
            rel_type3_p_to_ch = StringVar(self.parent)
            rel_type3_p_to_ch.set(self.rel_types.get_current_rel_types_for_category(str(category_name_str[2]), "p_to_ch"))
            self.rels3_entry_widget.config(textvariable=rel_type3_p_to_ch)
            self.rels3_entry_widget.grid(row=4, column=2, padx=2, pady=2, sticky="we")
            # child -> parent
            rel_type3_ch_to_p = StringVar(self.parent) 
            rel_type3_ch_to_p.set(self.rel_types.get_current_rel_types_for_category(str(category_name_str[2]), "ch_to_p"))
            self.rels3_entry_widget_inverted.config(textvariable=rel_type3_ch_to_p)
            self.rels3_entry_widget_inverted.grid(row=5, column=2, padx=2, pady=2, sticky="we")

        ### Rel type category 4
        Label(self.sem_types_toplevel, text="Type4: ").grid(row=6,column=0,sticky='e')
        self.type4_entry_widget = Entry(self.sem_types_toplevel, width=25)
        self.rels4_entry_widget = Entry(self.sem_types_toplevel, width=100)
        self.rels4_entry_widget_inverted = Entry(self.sem_types_toplevel, width=100)

        if len(category_name) > 3:
            ### Rel type category 4
            
            self.type4_entry_widget.config(textvariable=category_name[3])
            self.type4_entry_widget.grid(row=6, column=1, padx=2, pady=2, sticky='we')
            # parent -> child
            rel_type4_p_to_ch = StringVar(self.parent)
            rel_type4_p_to_ch.set(self.rel_types.get_current_rel_types_for_category(str(category_name_str[3]), "p_to_ch"))
            self.rels4_entry_widget.config(textvariable=rel_type4_p_to_ch)
            self.rels4_entry_widget.grid(row=6, column=2, padx=2, pady=2, sticky="we")
            # child - > parent
            rel_type4_ch_to_p = StringVar(self.parent) 
            rel_type4_ch_to_p.set(self.rel_types.get_current_rel_types_for_category(str(category_name_str[3]), "ch_to_p"))
            self.rels4_entry_widget_inverted.config(textvariable=rel_type4_ch_to_p)
            self.rels4_entry_widget_inverted.grid(row=7, column=2, padx=2, pady=2, sticky="we")

        option_list = ["parent to child", "child to parent"]
        if self.rel_types.is_direction_p_to_ch():
            option_value = option_list[0] 
        else:
            option_value = option_list[1]

        self.variable = StringVar(self.sem_types_toplevel, value=option_value)

        self.opt = OptionMenu(self.sem_types_toplevel, self.variable, *option_list)
        self.opt.config(width=18, height=1)
        self.opt.grid(row=8, column=1, sticky="e", padx=2, pady=2)

        Button(self.sem_types_toplevel, text="Accept Changes", width=20, height=1, underline=0, command=self.set_rel_type_values).grid(row=8, column=2, sticky="e", padx=2, pady=2)

        return "break"

    def setup_connection(self, event=None):
        self.connection_toplevel = Toplevel(self.parent)
        self.connection_toplevel.title("Database Connection")
        self.connection_toplevel.minsize(width=225, height=155)
        self.connection_toplevel.maxsize(width=225, height=180)
        self.connection_toplevel.transient(self.parent)

        v1 = StringVar(self.parent, value="e.g. 127.0.0.1")
        Label(self.connection_toplevel, text="Host ip:").grid(row=0,column=0,sticky='e')
        self.ip_entry_widget = Entry(self.connection_toplevel, width=25, textvariable=v1)
        self.ip_entry_widget.grid(row=0, column=1, padx=2, pady=2, sticky='we')

        self.ip_entry_widget.focus_set()

        v2 = StringVar(self.parent, value="e.g. 8000")
        Label(self.connection_toplevel, text="Port:").grid(row=1,column=0,sticky='e')
        self.port_entry_widget = Entry(self.connection_toplevel, width=25, textvariable=v2)
        self.port_entry_widget.grid(row=1, column=1, padx=2, pady=2, sticky='we')

        v3 = StringVar(self.parent, value="e.g. fetch_api")
        Label(self.connection_toplevel, text="URL suffix:").grid(row=2,column=0,sticky='e')
        self.url_entry_widget = Entry(self.connection_toplevel, width=25, textvariable=v3)
        self.url_entry_widget.grid(row=2, column=1, padx=2, pady=2, sticky='we')

        Label(self.connection_toplevel, text="username:").grid(row=3,column=0,sticky='e')
        self.user_entry_widget = Entry(self.connection_toplevel, width=25)
        self.user_entry_widget.grid(row=3, column=1, padx=2, pady=2, sticky='we')

        Label(self.connection_toplevel, text="password:").grid(row=4,column=0,sticky='e')
        self.pass_entry_widget = Entry(self.connection_toplevel, width=25)
        self.pass_entry_widget.grid(row=4, column=1, padx=2, pady=2, sticky='we')

        Button(self.connection_toplevel, text="Connect", underline=0, command=self.set_connection_values).grid(row=5, column=1, sticky="e"+ "w", padx=2, pady=2)

        return "break"

    def close_connection_window(self):
        self.connection_toplevel.destroy()

    def set_connection_values(self, event=None):   
        self.ip = self.ip_entry_widget.get()
        self.port = self.port_entry_widget.get()
        self.url_suffix = self.url_entry_widget.get()
        self.close_connection_window()
        self.controller.set_up_comm(self.ip, self.port, self.url_suffix)
        self.set_bottom_bar_action_value("db connection configured")
        self.sync_graph_meta()
    
    def set_bottom_bar_connection_value(self, connection_value):   
        if connection_value == CONN_STATUS[0]:
            self.connection_status_label.configure(text= CONN_STATUS[0])
            self.connection_status_label.configure(foreground="green")
        elif connection_value == CONN_STATUS[1]:
            self.connection_status_label.configure(text=CONN_STATUS[1])
            self.connection_status_label.configure(foreground="red")

    def set_bottom_bar_nodes_value(self, nodes_count):
        nodes_status = "<nodes: " + str(nodes_count) + ">"
        self.nodes_count_label.configure(text = nodes_status)

    def set_bottom_bar_rels_value(self, rels_count):
        rels_status = "<rels: " + str(rels_count) + ">"
        self.rels_count_label.configure(text = rels_status)

    def set_bottom_bar_action_value(self, last_action):
        last_action_text = "Last action: <" + str(last_action) + ">"
        self.last_action_label.configure(text=last_action_text)


    ### ADDITIONAL METHODS ###
    ##########################
   
    """
    Return a list of occurences of needle - i. e. pairs of indexes in "L.C" form 
    """
    def search_output(self, needle, if_ignore_case):
        #adjustments for more accurate search
        #needle = "<" + needle + ">"
        occurences = []
        matches_found = 0
        if needle:
            start_pos = '1.0'
            while True:
                start_pos = self.content_text.search(needle, start_pos,
                                                     nocase=if_ignore_case, stopindex=END)
                if not start_pos:
                    break 
                temp = str(start_pos).split(".")
                temp_l = int(temp[0])
                temp_c = int(temp[1])
                end_pos = '{}.{}'.format(temp_l, temp_c + len(needle))
                occurences.append((start_pos, end_pos))
                matches_found +=1
                start_pos = end_pos
        return occurences


    def set_tags_for_select1(self, needle_, if_ignore_case, local_tag, persistent_tag):
        occurences = self.search_output(needle_, if_ignore_case)
        for first,second in occurences:
            self.content_text.tag_add(local_tag, first, second)
            self.content_text.tag_add(persistent_tag, first, second)
            self.controller.node1_add_single_occurence(first, second)

    def set_tags_for_select2(self, needle_, if_ignore_case, local_tag, persistent_tag):
        occurences = self.search_output(needle_, if_ignore_case)
        for first,second in occurences:
            self.content_text.tag_add(local_tag, first, second)
            self.content_text.tag_add(persistent_tag, first, second)
            self.controller.node2_add_single_occurence(first, second)
        
    def select(self, event=None):
  
        if self.SELECT_COUNT == 0:
            content = self.content_text.selection_get()
            self.ENT1 = content

            first_index, second_index = self.content_text.tag_ranges(SEL)
            #self.controller.node1_add_single_occurence(first_index, second_index)  #current(manual) selection

            self.content_text.tag_add('current_sel1', first_index, second_index)
            self.content_text.tag_config('current_sel1', font=12,  offset="1", borderwidth="1", relief="solid", background="light yellow", foreground="black") 
            self.content_text.tag_config('current_sel1', font=("Arial",12, "bold"))
            self.content_text.tag_add("tag"+str(self.ID_COUNTER)+"_1", first_index, second_index)
            
            self.set_tags_for_select1(content, True, "current_sel1", "tag"+str(self.ID_COUNTER)+"_1")
            self.SELECT_COUNT += 1

        elif self.SELECT_COUNT == 1:
            content = self.content_text.selection_get()
            self.ENT2 = content

            first_index, second_index = self.content_text.tag_ranges(SEL)
            #self.controller.node2_add_single_occurence(first_index, second_index)   #current(manual) selection

            self.content_text.tag_add('current_sel2', first_index, second_index)
            self.content_text.tag_config("current_sel2", font=12,  offset="1", borderwidth="1", relief="solid", background="light green", foreground="black") 
            self.content_text.tag_config('current_sel2', font=("Arial",12, "bold"))
            self.content_text.tag_add("tag"+str(self.ID_COUNTER)+"_2", first_index, second_index)
            
            self.set_tags_for_select2(content, True, "current_sel2", "tag"+str(self.ID_COUNTER)+"_2")
            self.SELECT_COUNT +=1
        else:
            print("SELECT_COUNT: ", self.SELECT_COUNT)

        return "break"


    def unselect(self, event=None):  
        if self.SELECT_COUNT == 1:
            self.content_text.tag_delete('current_sel1')
            self.content_text.tag_delete("tag"+str(self.ID_COUNTER)+"_1")
            self.SELECT_COUNT -= 1
            self.ENT1 = None
            self.controller.node1_purge_occurences()

        elif self.SELECT_COUNT == 2:
            self.content_text.tag_delete('current_sel2')
            self.content_text.tag_delete("tag"+str(self.ID_COUNTER)+"_2")
            self.SELECT_COUNT -= 1
            self.ENT2 = None
            self.controller.node2_purge_occurences()
        else:
            print("SELECT_COUNT: ", self.SELECT_COUNT)

        return "break"

    def show_occurences(self, event=None):
        first_index, second_index = self.content_text.tag_ranges(SEL)
        content = self.content_text.selection_get()
        self.content_text.tag_add('occurences_sel', first_index, second_index)
        self.content_text.tag_config('occurences_sel', font="20",  offset="1", borderwidth="1", relief="solid", background="blue", foreground="white") 
        first_index = str(first_index)
        second_index = str(second_index)
        occurences = self.controller.get_other_occurences(first_index, second_index)
        if occurences == None:
            occurences = self.search_output(content, True)
        else:
            self.occurences_manipulatable = True
        if not occurences == None:
            for l,r in occurences:
                self.content_text.tag_add('occurences_sel', l,r) 
            

    def stop_showing_occurences(self, event=None):
        self.content_text.tag_delete('occurences_sel')
        self.occurences_manipulatable = False

    def get_index(self, string_index):
        string_index = str(string_index)
        def line_length(t, L):
            return int(t.index('%d.end'%L).split('.')[-1])
        temp = string_index.split('.')
        line_n = int(temp[0])
        index_in_line_n = int(temp[1])
        line_n_sum = 0
        for i in range(1,line_n):
            line_n_sum += line_length(self.content_text, i)
        return line_n_sum + index_in_line_n

    def add_occurence(self, event=None):
        first_index, second_index = self.content_text.tag_ranges(SEL)
        content = self.content_text.selection_get()
        first_index = str(first_index)
        second_index = str(second_index)
        self.content_text.tag_add('occurences_sel', first_index, second_index)
        self.controller.add_single_occurence_to_node(content, first_index, second_index)

    def remove_occurence(self, event=None):
        first_index, second_index = self.content_text.tag_ranges(SEL)
        first_index = str(first_index)
        second_index = str(second_index)
        self.controller.remove_single_occurence_from_node(first_index, second_index)
        self.content_text.tag_delete('occurences_sel')

        occurences = self.controller.get_other_occurences(first_index, second_index)
        if not occurences == None:
            for l,r in occurences:
                self.content_text.tag_add('occurences_sel', l,r) 
            self.content_text.tag_config('occurences_sel', font="20",  offset="1", borderwidth="1", relief="solid", background="blue", foreground="white") 

   
    def add_relationship(self, relationship_type):
        
        #self.tree.insert("","end", "id" + str(self.ID_COUNTER), text="id" + str(self.ID_COUNTER), values=(self.ENT1, relationship_type, self.ENT2))
    
        self.rel_tree.add_relationship(self.ID_COUNTER, relationship_type, self.ENT1, self.ENT2)
        self.controller.add_relationship(self.ID_COUNTER, relationship_type, self.ENT1, self.ENT2)
    
        self.content_text.tag_config("tag"+str(self.ID_COUNTER)+"_1", font="12",  offset="1", underline=1, background="light grey") 
        self.content_text.tag_config("tag"+str(self.ID_COUNTER)+"_1", font=("Arial",12, "bold"))
        self.content_text.tag_config("tag"+str(self.ID_COUNTER)+"_2", font="12",  offset="1", underline=1, background="light grey") 
        self.content_text.tag_config("tag"+str(self.ID_COUNTER)+"_2", font=("Arial",12, "bold"))
        
        tag_ranges1 = self.content_text.tag_ranges("tag"+str(self.ID_COUNTER)+"_1")
        tag_ranges2 = self.content_text.tag_ranges("tag"+str(self.ID_COUNTER)+"_2")
        temp = len(tag_ranges1)
        for i in range(0,len(tag_ranges1), 2):
            self.controller.add_vis_tag("tag"+str(self.ID_COUNTER)+"_1", tag_ranges1[i], tag_ranges1[i+1])
        for i in range(0,len(tag_ranges2), 2):
            self.controller.add_vis_tag("tag"+str(self.ID_COUNTER)+"_2", tag_ranges2[i], tag_ranges2[i+1])


        self.ID_COUNTER += 1
        self.content_text.tag_delete('current_sel1')
        self.content_text.tag_delete('current_sel2')
        self.SELECT_COUNT = 0
        self.ENT1 = None
        self.ENT2 = None

    def update_vis_tags(self):
        vis_tags = self.controller.get_all_gdoc_vis_tags()
        for tag in vis_tags:
            tag_id = tag["tag_id"]
            first_index = tag["left_border"]
            second_index = tag["right_border"]
            self.content_text.tag_add(tag_id, first_index, second_index)
            self.content_text.tag_config(tag_id, font="12", offset="1", underline=1, background="light grey")
            self.content_text.tag_config(tag_id, font=("Arial",12, "bold"))

    def show_popup_menu(self, event, popup_menu_):
        popup_menu_.tk_popup(event.x_root, event.y_root)


    def sync_graph_meta(self): 
        count_dict = self.controller.get_graph_meta()
        nodes_count = count_dict["nodes_count"]
        rels_count = count_dict["rels_count"]
        if not nodes_count == -1 and not rels_count == -1:
            self.set_bottom_bar_nodes_value(nodes_count)
            self.set_bottom_bar_rels_value(rels_count)
            self.set_bottom_bar_connection_value(CONN_STATUS[0])
        else:
            self.set_bottom_bar_connection_value(CONN_STATUS[1])
            
    def push_data(self):
        if self.details_all_complete():

            (line, c) = map(int, self.content_text.index("end-1c").split("."))
            content_lines = []
            for i in range(1,line+1):
                content_lines.append(self.content_text.get(str(i) + ".0", str(i+1)+ ".0-1c"))

            doc_name = self.document_name_entry.get()
            chap_name = self.chapter_name_entry.get()
            sent_num = self.sentences_number_entry.get()
            sect_num = self.sections_number_entry.get()
            sent_intentions = self.sentences_intentions_entry.get(1.0, END)
            sect_intentions = self.section_intentions_entry.get(1.0, END)
            self.controller.add_meta(doc_name, chap_name, sent_num, sect_num, sent_intentions, sect_intentions)
            self.controller.add_content_text(self.content_text.get("1.0", END), content_lines)
            if self.ip == None \
               or self.port == None \
               or self.url_suffix == None:
                self.setup_connection()
            self.controller.extract_content_text_graph()
            self.controller.push_data()
        else:
            
            self.document_name_v.set("Enter value!")
            self.chapter_name_v.set("Enter value!") 
            self.sentences_number_v.set("Enter value!")
            self.sections_number_v.set("Enter value!")
            self.sentences_intentions_entry.insert(1.0, "Enter value!")
            self.section_intentions_entry.insert(1.0, "Enter value!")
            
    def details_part_complete(self):
        
        doc_name = self.document_name_entry.get()
        chap_name = self.chapter_name_entry.get()
        if not len(doc_name) == 0 and not len(chap_name) == 0:
            return True
        else:
            return False

    def details_all_complete(self):
        
        doc_name = self.document_name_entry.get()
        chap_name = self.chapter_name_entry.get()
        sent_num = self.sentences_number_entry.get()
        sect_num = self.sections_number_entry.get()
        sent_intentions = self.sentences_intentions_entry.get(1.0, END)
        sect_intentions = self.section_intentions_entry.get(1.0, END)

        if not len(doc_name) == 0 and \
           not len(chap_name) == 0 and \
           not len(sent_num) == 0 and \
           not len(sect_num) == 0 and \
           not len(sent_intentions) == 0 and \
           not len(sect_intentions) == 0:
            return True 
        else:
            return False


    def save_to_file(self):

        if self.details_part_complete():
            self.controller.add_content_text(self.content_text.get("1.0", END), None)
            doc_name = self.document_name_entry.get()
            chap_name = self.chapter_name_entry.get()
            if self.sentences_number_entry.get() == None:
                sent_num = 0
            else:
                sent_num = self.sentences_number_entry.get()
            if self.sections_number_entry.get() == None:
                sect_num = 0
            else:
                sect_num = self.sections_number_entry.get()
            sent_intentions = self.sentences_intentions_entry.get(1.0, END)
            sect_intentions = self.section_intentions_entry.get(1.0, END)
            self.controller.add_meta(doc_name, chap_name, sent_num, sect_num, sent_intentions, sect_intentions)
            saveas_file_name = filedialog.asksaveasfilename(
                filetypes=[('Annotated Graph Document(*.arg)', '*.arg')], title="Save project as...")
            if saveas_file_name is None:
                return
            pickle.dump(self.controller.get_graph_document(), open(saveas_file_name, "wb"))
            self.set_bottom_bar_action_value("saved to file: " + os.path.basename(saveas_file_name))
            #filedialog.title(os.path.basename(saveas_file_name) + PROGRAM_NAME)
        else:
            
            self.document_name_v.set("Enter value!")
            self.chapter_name_v.set("Enter value!")

            

    def load_from_file(self):
        file_path = filedialog.askopenfilename(
            filetypes=[('Annotated Graph Document(*.arg)', '*.arg')], title='Load Project')
        if not file_path:
            return
        pickled_file_object = open(file_path, "rb")
        try:
            self.controller.add_graph_document(pickle.load(pickled_file_object))
        except EOFError:
            messagebox.showerror("Error",
                                 "Annotated Graph Document file seems corrupted or invalid !")
        pickled_file_object.close()
        try:
            #content = self.controller.model.model_content_text
            doc_name = self.controller.get_graph_doc_name()
            chap_name = self.controller.get_graph_chap_name()
            sent_num = str(self.controller.get_graph_sent_num()) 
            sect_num = str(self.controller.get_graph_sect_num())
            sent_intentions = self.controller.get_graph_sent_intentions_str() 
            sect_intentions = self.controller.get_graph_sect_intentions_str()

            self.document_name_v.set(doc_name)
            self.chapter_name_v.set(chap_name) 
            self.sentences_number_v.set(sent_num)
            self.sections_number_v.set(sect_num)
            self.sentences_intentions_entry.insert(1.0, sent_intentions)
            self.section_intentions_entry.insert(1.0, sect_intentions)

            #self.document_name_entry.set()
            content = self.controller.get_model_content_text()
            self.content_text.delete(1.0, "end")
            self.content_text.insert(1.0, content)
            self.set_bottom_bar_action_value("loaded file: " + os.path.basename(file_path))
            self.rel_tree.add_relationships_from_gdoc(self.controller.get_rels_from_gdoc())
            self.update_vis_tags()
            #self.root.title(os.path.basename(file_path) + PROGRAM_NAME)
        except:
            messagebox.showerror("Error",
                                 "An unexpected error occurred trying to process the Annotated Graph Document file")

    def sync_data(self):
        self.controller.add_content_text(self.content_text.get("1.0", END))
        #...

    def execute_query(self):  
        #self.rel_tree.tag_configure('bg', 'green')

        self.query_settings_toplevel = Toplevel(self.parent)
        self.query_settings_toplevel.title("Query settings")
        self.query_settings_toplevel.minsize(width=400, height=85)
        self.query_settings_toplevel.maxsize(width=400, height=85)
        self.query_settings_toplevel.transient(self.parent)

        self.query_label = Label(self.query_settings_toplevel, text="Enter a query parameter: ")
        self.query_label.grid(row=0, column=0, sticky="nw")
        self.query_settings_toplevel.grid_columnconfigure(0, weight=1)
        self.query_settings_toplevel.grid_columnconfigure(1, weight=1)

        self.query_param_entry = Entry(self.query_settings_toplevel)
        self.query_param_entry.grid(row=1, column=0, columnspan=2, pady=3, padx=3, sticky="we")

        option_list = ["VERIFY", "CALCULATE"]
        option_value = option_list[0]
        self.query_variable = StringVar(self.query_settings_toplevel, value=option_value)

        self.query_opt = OptionMenu(self.query_settings_toplevel, self.query_variable, *option_list)
        self.query_opt.config(width=18, height=1)
        self.query_opt.grid(row=2, column=0, pady=3, padx=3, sticky="we")
        #self.opt.grid(row=8, column=1, sticky="e", padx=2, pady=2)

        self.query_accept_button = Button(self.query_settings_toplevel, text="Run Query", command=self.accept_query_settings) 
        self.query_accept_button.grid(row=2, column=1, pady=3, padx=3, sticky="we")

        return "break"

    def accept_query_settings(self):

        query_param = self.query_param_entry.get()
        query_type = self.query_variable.get()

        (line, c) = map(int, self.content_text.index("end-1c").split("."))
        content_lines = []
        for i in range(1,line+1):
            content_lines.append(self.content_text.get(str(i) + ".0", str(i+1)+ ".0-1c"))

        self.controller.extract_content_text_graph()
        self.controller.push_query(query_param, query_type)
        self.query_settings_toplevel.destroy()
    




def main(model):
    root = Tk() 
    View(root, model)
    root.mainloop()


def init_new_app():
    initial_app_data = controller.Controller()
    main(initial_app_data)

if __name__ == "__main__":
    init_new_app()