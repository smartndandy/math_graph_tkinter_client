import requests
from models import ANode, ARelationship
import time
import json

#count: fetch_api/nodes_count/
#add nodes with relationships: fetch_api/nodes/




class Communicate:
    #base_url = "http://127.0.0.1:8000/fetch_api/" 

    def __init__(self, ip_, port_, url_suffix_):
        self.base_url = "http://" + ip_ + ":" + port_ + "/" + url_suffix_ + "/"

    def get_graph_meta(self):
        
        try:
            r = requests.get(f"{self.base_url}graph/") 
            r_json = r.json()
        except:
            r_json = {}

        result = {}
        if "nodes_count" in r_json and "rels_count" in r_json:
            result["nodes_count"] = r_json["nodes_count"]
            result["rels_count"] = r_json["rels_count"]
        else:
            result["nodes_count"] = -1
            result["rels_count"] = -1
        return result


    def post_rels_and_nodes(self, rels, nodes):

        temp_nodes = []
        temp_rels = []
        temp = {}

        for node in nodes:
            node_dict = {}
            node_dict["node_id"] = node.get_node_id()
            node_dict["term"] =  node.get_term_type()
            node_dict["node_type"] = node.get_node_type()
            temp_nodes.append(node_dict)

        for rel in rels:
            rel_dict = {}
            rel_dict["rel_id"] = rel.get_rel_id()
            rel_dict["a_type"] = rel.get_a_type() 
            rel_dict["node1"] = rel.get_node1().get_node_id() 
            rel_dict["node2"] = rel.get_node2().get_node_id()
            rel_dict["constraints"] = rel.get_constraints()
            temp_rels.append(rel_dict)

        temp["nodes"] = temp_nodes 
        temp["rels"] = temp_rels

        temp_json = json.dumps(temp)

        r = requests.post(f"{self.base_url}graph/", json=temp_json)
        print(r.status_code)


    def post_query(self, q_param, q_type, rels, nodes):
        
        temp_nodes = []
        temp_rels = []
        temp = {}

        for node in nodes:
            node_dict = {}
            node_dict["node_id"] = node.get_node_id()
            node_dict["term"] =  node.get_term_type()
            temp_nodes.append(node_dict)

        for rel in rels:
            rel_dict = {}
            rel_dict["rel_id"] = rel.get_rel_id()
            rel_dict["a_type"] = rel.get_a_type() 
            rel_dict["node1"] = rel.get_node1().get_node_id() 
            rel_dict["node2"] = rel.get_node2().get_node_id()
            temp_rels.append(rel_dict)

        temp["nodes"] = temp_nodes 
        temp["rels"] = temp_rels
        temp["query_type"] = q_type 
        temp["query_param"] = q_param

        temp_json = json.dumps(temp)

        r = requests.post(f"{self.base_url}query/", json=temp_json)
        print(r.status_code)
        
        


