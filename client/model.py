from communicate import Communicate
from models import ANodeOcc, ARelationship 


class GraphDocument:

    def __init__(self):
        # meta data attributes
        self.__doc_name = None 
        self.__chap_name = None 
        self.__sent_num = 0
        self.__sect_num = 0
        self.__sent_intentions_str = None 
        self.__sect_intentions_str = None

        self.sent_intentions_dict = {}
        self.sect_intentions_dict = {}

        self.vis_tags = []

        # relationships and nodes extracted manually
        self.semantic_relationships = []
        self.nodes = []

        # all graph data for comms 
        self.all_nodes_for_comm = []
        self.all_rels_for_comm = []

        # attributes for text processing
        self.content_text = None                    #string
        self.content_text_lines = None              #list of strings
        self.content_text_words_in_sentences = []     #list of lists of dictionaries
        
    def add_rels(self, id_counter, rel_type, ent1, ent2, occur_node1, occur_node2):

        a_node1 = self.node_in_nodes(occur_node1)
        if a_node1 == None:
            a_node1 = ANodeOcc(ent1, "id" + str(id_counter), "entity", occur_node1)
            self.nodes.append(a_node1)
        a_node2 = self.node_in_nodes(occur_node2)
        if a_node2 == None:
            a_node2 = ANodeOcc(ent2, "id" + str(id_counter), "entity", occur_node2)
            self.nodes.append(a_node2)

        a_relationship = ARelationship(rel_type, "id" + str(id_counter), a_node1, a_node2)
        self.semantic_relationships.append(a_relationship)


    def add_vis_tag(self, tag_id, left_border, right_border):
        temp_tag = {}
        temp_tag["tag_id"] = tag_id 
        temp_tag["left_border"] = float(str(left_border))
        temp_tag["right_border"] = float(str(right_border))
        self.vis_tags.append(temp_tag)

    def get_all_vis_tags(self):
        return list(self.vis_tags)

    def add_text(self, content, content_lines):
        self.content_text = content
        self.content_text_lines = content_lines

    def __extract_words_in_lines(self):

        self.content_text_words_in_sentences = []
        temp_sentence_list = []
        line_count = 1
        for line in self.content_text_lines:
            count = 0 
            word = ""
            word_dict = {}

            for letter in line:
                if letter == " " and len(word) > 0: 
                    if word[len(word)-1] == ".":
                        word = word[:-1]
                        word_right_index = (count - 1)
                        word_left_index = word_right_index - len(word)
                        word_dict[word] = (str(line_count) + "." + str(word_left_index), str(line_count) + "." + str(word_right_index))

                        temp_sentence_list.append(word_dict)
                        word_dict = {}      
                        word = ""  

                        self.content_text_words_in_sentences.append(temp_sentence_list)
                        temp_sentence_list = [] 
                    else:
                        word_right_index = count 
                        word_left_index = count - len(word)
                        word_dict[word] = (str(line_count) + "." + str(word_left_index), str(line_count) + "." + str(word_right_index))
                        temp_sentence_list.append(word_dict)
                        word_dict = {}      
                        word = ""   
                else:
                    word += letter
                    if count == (len(line) - 1):
                        if word[len(word)-1] == ".":
                            word = word[:-1]
                            word_right_index = (count - 1)
                            word_left_index = word_right_index - len(word)
                            word_dict[word] = (str(line_count) + "." + str(word_left_index), str(line_count) + "." + str(word_right_index))

                            temp_sentence_list.append(word_dict)
                            word_dict = {}      
                            word = ""  

                            self.content_text_words_in_sentences.append(temp_sentence_list)
                            temp_sentence_list = [] 
                        else:
                            word_right_index = count 
                            word_left_index = count - len(word)
                            word_dict[word] = (str(line_count) + "." + str(word_left_index), str(line_count) + "." + str(word_right_index))

                            temp_sentence_list.append(word_dict)
                            word_dict = {}      
                            word = ""  

                count += 1
            line_count += 1

    def add_constraint_to_rel_with_id(self, rel_id, constraint_val):
        rel_id = rel_id[2:]
        for elem in self.semantic_relationships:
            elem_id = elem.get_rel_id()
            if elem_id == rel_id:
                elem.set_constraints(constraint_val)

    def __extract_meta_and_intentions(self):

        sent_intentions_list = self.__sent_intentions_str.split(";")
        sent_intentions_list.pop()

        sect_intentions_list = self.__sect_intentions_str.split(";")
        sect_intentions_list.pop()

        self.__sent_intentions_dict = {}
        for elem in sent_intentions_list:
            sent = elem.split(":")[0]
            sent_type = elem.split(":")[1]
            self.__sent_intentions_dict[sent] = sent_type

        self.__sect_intentions_dict = {}
        for elem in sect_intentions_list:
            sect = elem.split(":")[0]
            sect_type = elem.split(":")[1]
            self.__sect_intentions_dict[sect] = sect_type


    def extract_content_text_graph(self):
        #a_node1 = ANodeOcc(ent1, "id" + str(id_counter), occur_node1)
        self.__extract_words_in_lines()
        self.__extract_meta_and_intentions()

        self.all_nodes_for_comm = []
        self.all_rels_for_comm = []

        sentence_nodes = [] 
        word_nodes = []
        #sect_intention_nodes = [] 
        sent_intention_nodes = []
        meta_nodes = []
        
        sentence_rels = []
        sentence_word_rels = []
        word_rels = []
        word_node_rels = []
        #sect_intention_rels = []
        sent_intention_rels = []
        meta_rels = []

        doc_node = ANodeOcc(self.__doc_name, "id_" + self.__doc_name, "meta_doc")
        chap_node = ANodeOcc(self.__chap_name, "id_" + self.__chap_name, "meta_chap") 
        meta_nodes.append(doc_node)
        meta_nodes.append(chap_node)
        doc_chap_rel = ARelationship("part_of", "id_" + self.__doc_name + "_" + self.__chap_name, chap_node, doc_node) 
        meta_rels.append(doc_chap_rel)

        sentence_count = 0
        for line_list in self.content_text_words_in_sentences:
            sentence_node = ANodeOcc("sent_" + str(sentence_count), "id_sent" + str(sentence_count), "meta_sentence")
            #section_node = ANodeOcc("sect_" + str(sentence_count), "id_sect" + str(sentence_count), "meta-section")
            meta_nodes.append(sentence_node)
            #meta_nodes.append(section_node)

            sent_intentions_key_list = list(self.__sent_intentions_dict.keys())
            if sentence_count+1 < len(sent_intentions_key_list):
                sent_intentions_key = sent_intentions_key_list[sentence_count]
                sentence_intention_node = ANodeOcc(self.__sent_intentions_dict[sent_intentions_key], "sent_intention" + str(sentence_count), "sent_intent")
                sent_intention_nodes.append(sentence_intention_node)

                chap_sent_rel = ARelationship("part_of", "id_" + self.__chap_name + "_" + sentence_node.get_term_type(), sentence_node, chap_node)
                meta_rels.append(chap_sent_rel)
                sent_sent_intent_rel = ARelationship("intention_of", "id_" + sentence_node.get_term_type() + "_" + sentence_intention_node.get_term_type(), sentence_intention_node, sentence_node)
                sent_intention_rels.append(sent_sent_intent_rel)
            #sect_intentions_key_list = list(self.__sect_intentions_dict.keys())
            #sect_intentions_key = sect_intentions_key_list[sentence_count]
            
            
            #section_intention_node = ANodeOcc(self.__sect_intentions_dict[sect_intentions_key], "sect_intention" + str(sentence_count), "sect_intent")
            #sect_intention_nodes.append(section_intention_node)

            #chap_sect_rel = ARelationship("part_of", "id_" + self.__chap_name + "_" + section_node.get_term_type(), section_node, chap_node)
            #meta_rels.append(chap_sect_rel)
            #sect_sect_intent_rel = ARelationship("intention_of", "id_" + section_node.get_term_type() + "_" + section_intention_node.get_term_type(), section_intention_node, section_node)
            #sect_intention_rels.append(sect_sect_intent_rel)
            
            

            word_count = 0
            for word in line_list:
                for word_c, word_occ in word.items():
                    word_node = ANodeOcc(word_c, "id_word" + str(sentence_count) + " " + str(word_count), "instance")
                    sentence_word_rel = ARelationship("sentence_flow_to", "id_" + str(word_node.get_node_id()) + "_" + str(sentence_node.get_node_id()), word_node, sentence_node)
                    sentence_word_rels.append(sentence_word_rel)

                    if not len(word_nodes) == 0:
                        previous_word_node = word_nodes[-1]
                        word_rel = ARelationship("flow_to", "id_" + str(word_node.get_node_id()) + "_" + str(previous_word_node.get_node_id()), word_node, previous_word_node)
                        word_rels.append(word_rel)
                    
                    word_nodes.append(word_node) 
                    for node in self.nodes:
                        for node_occ in node.get_all_occurences():
                            if node_occ == word_occ:
                                word_node_rel = ARelationship("ex_of", "id_" + str(word_node.get_node_id()) + "_" + str(node.get_node_id()), word_node, node)
                                word_node_rels.append(word_node_rel)

                word_count += 1

            sentence_nodes.append(sentence_node)
            sentence_count += 1

        print(sentence_nodes)

        self.all_nodes_for_comm = list(self.nodes)
        self.all_rels_for_comm = list(self.semantic_relationships)

        """
        sect_intention_rels = []
        sent_intention_rels = []
        meta_rels = []
        """

        #self.all_nodes_for_comm += sentence_nodes
        self.all_nodes_for_comm += word_nodes
        #self.all_nodes_for_comm += sect_intention_nodes
        self.all_nodes_for_comm += sent_intention_nodes 
        self.all_nodes_for_comm += meta_nodes

        self.all_rels_for_comm += sentence_rels 
        self.all_rels_for_comm += sentence_word_rels 
        self.all_rels_for_comm += word_rels 
        self.all_rels_for_comm += word_node_rels
        #self.all_rels_for_comm += sect_intention_rels 
        self.all_rels_for_comm += sent_intention_rels 
        self.all_rels_for_comm += meta_rels

    def add_meta(self, doc_name, chap_name, sent_num, sect_num, sent_intentions, sect_intentions):
        self.__doc_name = doc_name 
        self.__chap_name = chap_name 
        if sent_num == "":
            self.__sent_num = 0
        else:
            self.__sent_num = int(sent_num)
        if sect_num == "":
            self.__sect_num = 0
        else:
            self.__sect_num = int(sect_num)
        
        self.__sent_intentions_str = sent_intentions 
        self.__sect_intentions_str = sect_intentions

    def get_graph_doc_name(self):
        return self.__doc_name 
    
    def get_graph_chap_name(self):
        return self.__chap_name 

    def get_graph_sent_num(self):
        return self.__sent_num 
    
    def get_graph_sect_num(self):
        return self.__sect_num 

    def get_graph_sent_intentions_str(self):
        return self.__sent_intentions_str 
    
    def get_graph_sect_intentions_str(self):
        return self.__sect_intentions_str
    
    def get_all_rels_for_comm(self):
        return list(self.all_rels_for_comm)

    def get_all_nodes_for_comm(self):
        return list(self.all_nodes_for_comm)

    def get_all_rels_for_comm(self):
        return list(self.all_rels_for_comm)

    def get_nodes(self):
        return list(self.nodes) 

    def get_rels(self):
        return list(self.semantic_relationships)

    def get_text(self):
        return self.content_text

    def node_in_nodes(self, occurences):
        result = None
        for node in self.nodes:
            if node.get_all_occurences() == occurences:
                result = node 
        return result
                
    def find_node_by_occurence(self, left, right):
        result = None
        for node in self.nodes:
            if node.has_occurence(left, right):
                result = node.get_all_occurences()
                
        if result == None:
            return result
        else: 
            return list(result)

    def add_single_occurence_to_node(self, content, left, right):
        for node in self.nodes:
            if content == node.get_term_type():
                node.add_occurence(left,right)
        
    def remove_single_occurence_from_node(self, left, right):
        for node in self.nodes:
            if node.has_occurence(left, right):
                node.remove_occurence(left,right)
    
        

class Model:

    def __init__(self):
        self.g_doc = GraphDocument()
        self.comm = None
        self.node1_occurences = [] 
        self.node2_occurences = []

    def add_relationship(self, id_counter, rel_type, ent1, ent2):
        self.g_doc.add_rels(id_counter, rel_type, ent1, ent2, self.node1_occurences, self.node2_occurences)
        self.node1_purge_occurences()
        self.node2_purge_occurences()

    def add_content_text(self, content, content_list):
        self.g_doc.add_text(content, content_list)
        #content_list = content.split(" ")
        #print(content_list)

    def add_vis_tag(self, tag_id, left_border, right_border):
        self.g_doc.add_vis_tag(tag_id, left_border, right_border)

    def get_all_gdoc_vis_tags(self):
        return list(self.g_doc.get_all_vis_tags())

    def get_model_content_text(self):
        return self.g_doc.get_text()

    def get_rels_from_gdoc(self):
        return self.g_doc.get_rels()

    def node1_add_single_occurence(self, left, right):
        elem = (str(left),str(right))
        if not elem in self.node1_occurences:
            self.node1_occurences.append((left,right))

    def node1_remove_single_occurence(self, left, right):
        elem = (left, right)
        self.node1_occurences.remove(elem)

    def add_constraint_to_rel_with_id(self, rel_id, constraint_val):
        self.g_doc.add_constraint_to_rel_with_id(rel_id, constraint_val)

    def node2_add_single_occurence(self, left, right):
        elem = (str(left),str(right))
        if not elem in self.node2_occurences:
            self.node2_occurences.append((left,right))

    def node2_remove_single_occurence(self, left, right):
        elem = (left, right)
        self.node2_occurences.remove(elem)

    def node1_purge_occurences(self):
        self.node1_occurences = []

    def node2_purge_occurences(self):
        self.node2_occurences = []

    def get_other_occurences(self, left, right):
        result = self.g_doc.find_node_by_occurence(left, right)
        if result == None:
            return result
        else: 
            return list(result)

    def add_meta(self, doc_name, chap_name, sent_num, sect_num, sent_intentions, sect_intentions):
        self.g_doc.add_meta(doc_name, chap_name, sent_num, sect_num, sent_intentions, sect_intentions)

    def get_graph_doc_name(self):
        return self.g_doc.get_graph_doc_name() 

    def get_graph_chap_name(self):
        return self.g_doc.get_graph_chap_name() 

    def get_graph_sent_num(self):
        return self.g_doc.get_graph_sent_num() 

    def get_graph_sect_num(self):
        return self.g_doc.get_graph_sect_num()

    def get_graph_sent_intentions_str(self):
        return self.g_doc.get_graph_sent_intentions_str() 

    def get_graph_sect_intentions_str(self):
        return self.g_doc.get_graph_sect_intentions_str()

    def add_single_occurence_to_node(self, content, left, right):
        self.g_doc.add_single_occurence_to_node(content,left,right)

    def remove_single_occurence_from_node(self, left, right):
        self.g_doc.remove_single_occurence_from_node(left, right)

    def set_up_comm(self, ip, port, url_suffix):
        self.comm = Communicate(ip, port, url_suffix)

    def get_graph_document(self):
        return self.g_doc

    def add_graph_document(self, graph_doc):
        self.g_doc = graph_doc

    def post_data(self):
        self.comm.post_rels_and_nodes(self.g_doc.get_all_rels_for_comm(), self.g_doc.get_all_nodes_for_comm())

    def post_query(self, q_param, q_type):
        self.comm.post_query(q_param, q_type, self.g_doc.get_all_rels_for_comm(), self.g_doc.get_all_nodes_for_comm())

    def extract_content_text_graph(self):
        self.g_doc.extract_content_text_graph()

    def get_graph_meta(self):
        return self.comm.get_graph_meta()
    
