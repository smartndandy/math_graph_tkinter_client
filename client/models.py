import time


def unique_id(initial_id):
        current_time = time.perf_counter_ns()
        return initial_id + "_" + str(current_time)

class ARelationship:

    def __init__(self, a_type_, def_id, node1_, node2_):
        self.a_type = a_type_
        self.rel_id = unique_id(def_id)
        self.node1 = node1_
        self.node2 = node2_ 
        self.constraints = " "
        
    def get_constraints(self):
        return self.constraints

    def set_constraints(self, cs):
        self.constraints = cs

    def get_a_type(self):
        return self.a_type

    def get_rel_id(self):
        return self.rel_id

    def get_node1(self):
        return self.node1 

    def get_node2(self):
        return self.node2

    
class ANode:

    def __init__(self, term_, node_id_):
        self.term = term_
        self.node_id = unique_id(node_id_) + "_n"

    def get_term_type(self):
        return self.term 

    def get_node_id(self):
        return self.node_id


class ANodeOcc(ANode):

    def __init__(self, term_, node_id_, node_type, occur_ = []):
        super().__init__(term_, node_id_)
        self.node_type = node_type
        self.occurences = occur_

    def add_occurence(self, left, right):
        self.occurences.append((left,right))

    def remove_occurence(self, left, right):
        elem = (left,right)
        self.occurences.remove(elem) 

    def get_node_type(self):
        return self.node_type

    def has_occurence(self, left, right):
        elem = (left, right)
        if elem in self.occurences:
            return True 
        else:
            return False

    def get_all_occurences(self):
        return list(self.occurences)