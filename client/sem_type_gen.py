import json
from arg_constants import *

def turn_to_sem_list(rel_list):
    descr_list = []
    for first,second in rel_list:
        temp = {}
        temp["p_to_ch"] = first
        temp["ch_to_p"] = second
        descr_list.append(temp) 
    return descr_list



def generate_json():

    temp_dict = {} 

    descr_dict = {}
    

    descr_dict[RELATIONSHIP_CATEGORIES[0]] = turn_to_sem_list(DESCR_RELS_REGULAR)

    comp_dict = {}
    comp_dict[RELATIONSHIP_CATEGORIES[1]] = turn_to_sem_list(COMP_RELS_REGULAR)

    cat_dict = {}
    cat_dict[RELATIONSHIP_CATEGORIES[2]] = turn_to_sem_list(CAT_RELS_REGULAR)

    func_dict = {}
    func_dict[RELATIONSHIP_CATEGORIES[3]] = turn_to_sem_list(FUNC_RELS_REGULAR)

    temp_dict["0"] = descr_dict 
    temp_dict["1"] = comp_dict
    temp_dict["2"] = cat_dict 
    temp_dict["3"] = func_dict

    data = temp_dict 
    with open("client/configs/default_sem_types.json", "w") as fp:
            json.dump(data, fp, indent=4)




if __name__ == "__main__":
    generate_json()