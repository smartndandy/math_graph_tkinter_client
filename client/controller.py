import model
import copy

class Controller():

    def __init__(self):
        self.init_model()

    def init_model(self):
        self.model = model.Model()

    def add_relationship(self, id_counter, rel_type, ent1, ent2):
        self.model.add_relationship(id_counter, rel_type, ent1, ent2)

    def get_rels_from_gdoc(self):
        return self.model.get_rels_from_gdoc()
    
    def get_all_gdoc_vis_tags(self):
        return list(self.model.get_all_gdoc_vis_tags())

    def add_vis_tag(self, tag_id, left_border, right_border):
        self.model.add_vis_tag(tag_id, left_border, right_border)

    def add_content_text(self, content, content_list):
        self.model.add_content_text(content, content_list)

    def set_up_comm(self, ip, port, url_suffix):
        self.model.set_up_comm(ip, port, url_suffix)

    def get_graph_meta(self):
        return self.model.get_graph_meta()

    def push_data(self):     
        self.model.post_data()

    def push_query(self, query_param, query_type):
        self.model.post_query(query_param, query_type)

    def get_graph_document(self):
        return self.model.get_graph_document()

    def add_graph_document(self, graph_doc):
        return self.model.add_graph_document(graph_doc)

    def add_meta(self, doc_name, chap_name, sent_num, sect_num, sent_intentions, sect_intentions):
        self.model.add_meta(doc_name, chap_name, sent_num, sect_num, sent_intentions, sect_intentions)

    ### graph meta data access functions

    def get_graph_doc_name(self):
        return self.model.get_graph_doc_name() 

    def get_graph_chap_name(self):
        return self.model.get_graph_chap_name() 

    def get_graph_sent_num(self):
        return self.model.get_graph_sent_num()

    def get_graph_sect_num(self):
        return self.model.get_graph_sect_num() 

    def get_graph_sent_intentions_str(self):
        return self.model.get_graph_sent_intentions_str()

    def get_graph_sect_intentions_str(self):
        return self.model.get_graph_sect_intentions_str()

    ### end: graph meta data access functions

    def get_model_content_text(self):
        return self.model.get_model_content_text()

    def node1_add_single_occurence(self, left, right):
        self.model.node1_add_single_occurence(left, right)

    def node1_remove_single_occurence(self, left, right):
        self.model.node1_remove_single_occurence(left, right)

    def node2_add_single_occurence(self, left, right):
        self.model.node2_add_single_occurence(left, right)

    def node2_remove_single_occurence(self, left, right):
        self.model.node2_remove_single_occurence(left, right) 

    def node1_purge_occurences(self):
        self.model.node1_purge_occurences()

    def node2_purge_occurences(self):
        self.model.node2_purge_occurences()

    def get_other_occurences(self, left,right):
        result = self.model.get_other_occurences(left, right)
        if result == None:
            return result
        else: 
            return list(result)

    def add_single_occurence_to_node(self, content, left, right):
        self.model.add_single_occurence_to_node(content, left, right)

    def add_constraint_to_rel_with_id(self, rel_id, constraint_val):
        self.model.add_constraint_to_rel_with_id(rel_id, constraint_val)
    
    def extract_content_text_graph(self):
        self.model.extract_content_text_graph()

    def remove_single_occurence_from_node(self, left, right):
        self.model.remove_single_occurence_from_node(left, right)