from tkinter import *
master = Tk()

file_path = r"C:\Users\prdie\Google Drive\Sources\arg0_01_client\arg0_01_tkinter_client\text samples\1_2_Geraden_und_Ebenen\kp1_2.txt"

list = [file_path, ]


def _toggle_visibility(event, widg):
    global txt
    try:
        block_start, block_end = _get_block("insert", widg)
    except:
        return
    next_hidden = widg.tag_nextrange("hidden", block_start, block_end)
    if next_hidden:
        widg.tag_remove("hidden", block_start, block_end)
    else:
        widg.tag_add("hidden", block_start, block_end)
def _get_block(index, widg):
    global txt
    '''return indicies after header, to next header or EOF'''
    start = widg.index("%s lineend+1c" % index)
    next_header = widg.tag_nextrange("header", start)
    if next_header:
        end = next_header[0]
    else:
        end = widg.index("end-1c")
    return (start, end)

txt = Text(master)
txt.grid()
txt.tag_configure("header", foreground="#9b3e96")  # , spacing1=10, spacing3=10)
txt.tag_bind("header", "<Double-1>", lambda event: _toggle_visibility(event, txt))
txt.tag_configure("hidden", elide=True)


for item in list:
    with open(item) as f:
        h = f.readlines()
        txt.insert('end', '==============================================\n ', 'header')
        txt.insert('end', h)
master.mainloop()