## description:
The intention behind this project was to create a testbed for experimenting with graph-based document representations. This is a simple tkinter client
application that is used to communicate with a Django-backend using a Neo4j database. (the backend code can be found in the math_graph_backend repo)

This gui application allows to specify types of semantic relations in the settings and then annotate a text document by selecting strings and choosing a particular relation. 
The created relation entity triples can be seen in a treeview widget. In addition to the user added semantic relationships the graph consists of additional nodes and edges that
capture the text itself and additional meta information about the document.

A button and dialog to run queries is added but the functionality is not yet implemented. The hope is that using a graph based representation similar to this together with
different graph-traversal algorithms (etc.) will allow applications such as semantic search, document comparison, semantic auto-correction and others.

There are most definitely bugs and this is still very much work in progress.

## annotation:
![Annotated text document in client](https://bitbucket.org/smartndandy/math_graph_tkinter_client/raw/f293d66f8a1920e7def19e739c573f533b2683fa/screenshots/document_annotation.png)


## graph representation:
![Graph](https://bitbucket.org/smartndandy/math_graph_tkinter_client/raw/f293d66f8a1920e7def19e739c573f533b2683fa/screenshots/document_representation.png)