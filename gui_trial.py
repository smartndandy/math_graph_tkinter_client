import tkinter as tk

# callback functions

def callback_button1():
    print(str(find_string.get()))

def callback_button2():
    print(str(replace_string.get()))

def show_help(event):
    print("Showing help...")


parent = tk.Tk()
parent.title("Find & Replace")
parent.bind_all('<Shift-Control-KeyPress-H>', show_help)

find_string = tk.StringVar()
replace_string = tk.StringVar()

tk.Label(parent, text="Find:").grid(row=0, column=0, sticky='e')
tk.Entry(parent, width=60, textvariable=find_string).grid(row=0, column=1, padx=2, pady=2, sticky='we', columnspan=9)

tk.Label(parent, text="Replace:").grid(row=1, column=0, sticky='e')
tk.Entry(parent, width=60, textvariable=replace_string).grid(row=1, column=1, padx=2, pady=2, sticky='we', columnspan=9)

tk.Button(parent, text="Find", command=callback_button1).grid(row=0, column=10, sticky='ew', padx=2, pady=2)
tk.Button(parent, text="Find All", command=callback_button2).grid(row=1, column=10, sticky='ew', padx=2)


parent.mainloop()