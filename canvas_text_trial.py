
import tkinter as tk
 
root = tk.Tk()
 
file_path = r"text samples\1_1_Vektoren_im_Anschauungsraum\kp1_1_group_specifiers.txt"


list_of_lines = []
list_of_words_in_line = []
with open(file_path, "r") as text:
    lines = text.readlines() 

    for line in lines:
        #list_of_words_in_line = []
        """
        for word in line:
            list_of_lines.append(word)
        """
        list_of_words_in_line.append(line.split(" "))


canvas_width = 600
canvas_height = 1000
#w = canvas_width // 2
#h = canvas_height // 2
#canvas = tk.Canvas(root, width=canvas_width, height=canvas_height)
#canvas.pack()
 
frame=tk.Frame(root,width=canvas_width+20,height=canvas_height+20)
frame.pack(expand=True, fill=tk.BOTH) #.grid(row=0,column=0)
canvas=tk.Canvas(frame,bg='#FFFFFF',width=canvas_width,height=canvas_height,scrollregion=(0,0,1000,1000))
hbar=tk.Scrollbar(frame,orient=tk.HORIZONTAL)
hbar.pack(side=tk.BOTTOM,fill=tk.X)
hbar.config(command=canvas.xview)
vbar=tk.Scrollbar(frame,orient=tk.VERTICAL)
vbar.pack(side=tk.RIGHT,fill=tk.Y)
vbar.config(command=canvas.yview)
canvas.config(width=canvas_width,height=canvas_height)
canvas.config(xscrollcommand=hbar.set, yscrollcommand=vbar.set)
canvas.pack(side=tk.LEFT,expand=True,fill=tk.BOTH)


rect_width = 40
rect_height = 10
"""
h = 5

text_list = []
for i in range(200):
    line_list = []
    w = 2
    h += (rect_height + 5)
    for j in range(10):

        w += (rect_width + 5)
        rectangle = canvas.create_rectangle(w, h, w + rect_width, h + rect_height)
        rectangle_text = canvas.create_text(w + rect_width / 2, h + rect_height / 2, text="Hello")

        line_list.append((rectangle, rectangle_text))
    text_list.append(line_list)
"""
def onObjectClick(event):                  
    print('Got object click', event.x, event.y)
    widg = event.widget.find_closest(event.x, event.y)
    #widg.configure(activebackground = "#33B5E5", relief = tk.FLAT)
    print(event.widget.find_closest(event.x, event.y))

h = 5
for elem in list_of_words_in_line:
    w = 2
    h += (rect_height + 5)
    for el in elem:
        w += (rect_width + 5)
        rectangle = canvas.create_rectangle(w, h, w + rect_width, h + rect_height, tags="tag_rect"+str(w)+"_"+str(h))
        rectangle_text = canvas.create_text(w + rect_width / 2, h + rect_height / 2, text=el, tags="tag_text"+str(w)+"_"+str(h))
        canvas.tag_bind("tag_text"+str(w)+"_"+str(h), "<ButtonPress-1>", onObjectClick)

#r1 = canvas.create_rectangle(w, h, w + 40, h + 10)
#t = canvas.create_text(w + 20, h + 5, text="Hello")
 
"""
def keypress(event):
    The 4 key press
    x, y = 0, 0
    if event.char == "a":
        x = -10
    if event.char == "d":
        x = 10
    if event.char == "w":
        y = -10
    if event.char == "s":
        y = 10
    canvas.move(r1, x, y)
    canvas.move(t, x, y)
 
 
root.bind("<Key>", keypress)
"""
root.mainloop()